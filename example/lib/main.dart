import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:inapp_image_picker/inapp_image_picker.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Builder(
            builder: (context) => RaisedButton(
              child: Text("Select picture"),
              onPressed: () {
                InAppImagePickerBottomSheet.show(
                  context,
                  mediaTypes: [MediaType.video],
                  permissionAccessText: "Gave me access to your storage",
                  title: "Pick image",
                  multiSelect: false,
                  actionButtonIcon: Icon(Icons.check),
                  actionButtonText: Text("Next"),
                  onActionButtonTap: (s) {
                    print(s);
                  },
                  permissionAlert: Text("Gave me access"),
                  placeHolder:
                      NetworkImage('https://undraw.co/apple-touch-icon.png'),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
