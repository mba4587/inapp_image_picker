
class PermissionModel {
  final bool isUndetermined;
  final bool isRestricted;
  final bool isGranted;

  PermissionModel({this.isUndetermined, this.isRestricted, this.isGranted});
}