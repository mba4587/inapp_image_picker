import 'package:permission_handler/permission_handler.dart';

import 'model.dart';

class StoragePermission {
  static const Permission permission = Permission.storage;

  static Future<PermissionModel> checkPermission() async {
    bool isUndetermined = false;
    bool isRestricted = false;
    bool isGranted = false;
    var status = await permission.status;
    if (status.isUndetermined) {
      isUndetermined = true;
    }
    if (await permission.isRestricted) {
      isRestricted = true;
    }
    if (await permission.isGranted) {
      isGranted = true;
    }

    return PermissionModel(
        isRestricted: isRestricted,
        isUndetermined: isUndetermined,
        isGranted: isGranted);
  }

  static Future<bool> requestForPermission() async {
    PermissionModel permissionModel = await checkPermission();
    if (!permissionModel.isGranted) {
      Map<Permission, PermissionStatus> status = await [permission].request();
      if(status[permission] == PermissionStatus.granted) {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }
}
