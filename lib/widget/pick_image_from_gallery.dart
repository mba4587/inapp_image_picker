import 'package:flutter/material.dart';
import 'package:inapp_image_picker/permissions/storage.dart';
import 'package:inapp_image_picker/widget/show_material_bottom_shit.dart';
import 'package:inapp_image_picker/widget/sized_box.dart';
import 'package:media_gallery/media_gallery.dart';

import 'custom_list_tile.dart';

class InAppImagePickerBottomSheet {
  static List<MediaCollection> collections = [];

  static show(context,
      {multiSelect = false,
      Function(List<Media> selected) onActionButtonTap,
      Widget permissionAlert,
      String permissionAccessText,
      Widget actionButtonText,
      Widget actionButtonIcon,
      String title,
      @required ImageProvider placeHolder,
      List<MediaType> mediaTypes = const [MediaType.image]}) async {
    if (await StoragePermission.requestForPermission()) {
      collections = await MediaGallery.listMediaCollections(
        mediaTypes: mediaTypes,
      );

      final MediaPage imagePage = await collections[0].getMedias(
        mediaType: (mediaTypes[0] == MediaType.image)
            ? MediaType.image
            : MediaType.video,
        take: 500,
      );

      final List<Media> allMedias = [
        ...imagePage.items,
      ]..sort((x, y) => y.creationDate.compareTo(x.creationDate));

      showCustomBottomSheet(
        context,
        expand: false,
        bounce: true,
        builder: (context, scrollController) {
          return Container(
            height: MediaQuery.of(context).size.height - 100,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SpaceY(16),
                Container(
                  height: 3,
                  width: 50,
                  decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                    borderRadius: BorderRadius.circular(16),
                  ),
                ),
                SpaceY(8),
                Text(
                  title,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.all(16),
                    child: GridImageGallery(
                      allMedias: allMedias,
                      onActionClick: onActionButtonTap,
                      multiSelect: multiSelect,
                      actionButtonText: actionButtonText,
                      actionButtonIcon: actionButtonIcon,
                      placeholder: placeHolder,
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      );
    } else {
      showCustomBottomSheet(
        context,
        builder: (context, scrollController) {
          return Padding(
            padding: EdgeInsets.all(16),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                permissionAlert,
                CustomListTile(
                  title: Text(
                    permissionAccessText,
                    textDirection: TextDirection.rtl,
                  ),
                  trailing: Icon(Icons.storage),
                  onTap: () {
                    Navigator.of(context).pop();
                    show(context,
                        placeHolder: placeHolder,
                        permissionAlert: permissionAlert,
                        actionButtonText: actionButtonText,
                        actionButtonIcon: actionButtonIcon,
                        multiSelect: multiSelect,
                        title: title,
                        permissionAccessText: permissionAccessText,
                        mediaTypes: mediaTypes,
                        onActionButtonTap: onActionButtonTap);
                  },
                  radius: BorderRadius.circular(16),
                )
              ],
            ),
          );
        },
      );
    }
  }
}

class GridImageGallery extends StatefulWidget {
  final List<Media> allMedias;
  final bool multiSelect;
  final Function(List<Media> selected) onActionClick;
  final Widget actionButtonText;
  final Widget actionButtonIcon;
  final ImageProvider placeholder;

  const GridImageGallery(
      {Key key,
      this.allMedias,
      this.multiSelect,
      this.actionButtonText,
      this.actionButtonIcon,
      this.onActionClick,
      @required this.placeholder})
      : super(key: key);

  @override
  _GridImageGalleryState createState() => _GridImageGalleryState();
}

class _GridImageGalleryState extends State<GridImageGallery> {
  List<Media> selectedList = [];
  Media selected;

  ImageProvider placeholder;

  @override
  void initState() {
    super.initState();
    if (widget.placeholder != null) {
      placeholder = widget.placeholder;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Expanded(
          child: GridView.count(
            crossAxisCount: 3,
            mainAxisSpacing: 2,
            crossAxisSpacing: 2,
            children: [
              for (int i = 0; i < widget.allMedias.length; i++) ...[
                Stack(
                  overflow: Overflow.visible,
                  fit: StackFit.expand,
                  children: [
                    GestureDetector(
                      onTap: () {
                        if (widget.multiSelect) {
                          if (selectedList.contains(widget.allMedias[i])) {
                            setState(() {
                              selectedList.remove(widget.allMedias[i]);
                            });
                          } else {
                            setState(() {
                              selectedList.add(widget.allMedias[i]);
                            });
                          }
                        } else {
                          if (selected == widget.allMedias[i]) {
                            setState(() {
                              selected = null;
                            });
                          } else {
                            setState(() {
                              selected = widget.allMedias[i];
                            });
                          }
                        }
                      },
                      child: AnimatedContainer(
                        duration: Duration(milliseconds: 200),
                        margin: widget.multiSelect
                            ? selectedList.contains(widget.allMedias[i])
                                ? EdgeInsets.all(8)
                                : EdgeInsets.zero
                            : selected == widget.allMedias[i]
                                ? EdgeInsets.all(8)
                                : EdgeInsets.zero,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: FadeInImage(
                            fit: BoxFit.cover,
                            placeholder: placeholder,
                            image: MediaThumbnailProvider(
                              media: widget.allMedias[i],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 2,
                      right: 2,
                      child: AnimatedContainer(
                        width: (!widget.multiSelect)
                            ? (selected == widget.allMedias[i]) ? 24 : 0
                            : (selectedList.contains(widget.allMedias[i]))
                                ? 8
                                : 0,
                        height: (!widget.multiSelect)
                            ? (selected == widget.allMedias[i]) ? 24 : 0
                            : (selectedList.contains(widget.allMedias[i]))
                                ? 8
                                : 0,
                        padding: EdgeInsets.all(1),
                        decoration: BoxDecoration(
                            color: Theme.of(context).accentColor,
                            borderRadius: BorderRadius.circular(25)),
                        duration: Duration(milliseconds: 200),
                        child: Center(
                          child: _showIcon(i)
                              ? Icon(
                                  Icons.check,
                                  color: Colors.white,
                                  size: 20,
                                )
                              : SizedBox.shrink(),
                        ),
                      ),
                    )
                  ],
                )
              ]
            ],
          ),
        ),
        Positioned(
          bottom: 16,
          right: 16,
          child: AnimatedContainer(
            duration: Duration(milliseconds: 200),
            width: (selectedList.length > 0 || selected != null) ? null : 0,
            height: (selectedList.length > 0 || selected != null) ? null : 0,
            child: (selectedList.length > 0 || selected != null)
                ? FloatingActionButton.extended(
                    onPressed: () {
                      if (!widget.multiSelect) {
                        selectedList.clear();
                        selectedList.add(selected);
                      }
                      if (widget.onActionClick != null)
                        widget.onActionClick(selectedList);
                    },
                    label: widget.actionButtonText,
                    icon: widget.actionButtonIcon,
                  )
                : SizedBox.shrink(),
          ),
        ),
      ],
    );
  }

  _showIcon(i) {
    return (!widget.multiSelect)
        ? (selected == widget.allMedias[i]) ? true : false
        : (selectedList.contains(widget.allMedias[i])) ? true : false;
  }
}
