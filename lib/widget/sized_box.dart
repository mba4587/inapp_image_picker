import 'package:flutter/material.dart';

class SpaceY extends StatelessWidget {
  final double size;

  const SpaceY(this.size, {Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: size,
    );
  }
}

class SpaceX extends StatelessWidget {
  final double size;

  const SpaceX(this.size, {Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: size,
    );
  }
}
