import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

showCustomBottomSheet(context,
    {Function(BuildContext context, ScrollController scrollController) builder,
    bool expand = false,
    bool bounce = false}) {
  return showMaterialModalBottomSheet(
    context: context,
    backgroundColor: Theme.of(context).primaryColorDark,
    elevation: 8,
    expand: expand,
    bounce: bounce,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topRight: Radius.circular(16),
        topLeft: Radius.circular(16),
      ),
    ),
    builder: (context, scrollController) {
      return builder(context, scrollController);
    },
  );
}
