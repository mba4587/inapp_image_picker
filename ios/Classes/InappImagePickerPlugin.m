#import "InappImagePickerPlugin.h"
#if __has_include(<inapp_image_picker/inapp_image_picker-Swift.h>)
#import <inapp_image_picker/inapp_image_picker-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "inapp_image_picker-Swift.h"
#endif

@implementation InappImagePickerPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftInappImagePickerPlugin registerWithRegistrar:registrar];
}
@end
